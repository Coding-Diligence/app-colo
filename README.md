# Colo Loca

Colo Loca is a site designed to enhance the efficiency of living with other people in a colocation. It offers features such as:
- Task management where you can see all tasks and who needs to do them.
- A unique profile in the colocation where you can see only your tasks.
- An expenses section where you can track financial matters like paying bills and other expenditures.

## Installation

To set up and run the Colo Loca application, follow these steps:

1. **Clone the Repository**
   ```sh
   git clone https://gitlab.com/Coding-Diligence/app-colo.git
   cd app-colo
   ```

## Backend Setup
Using nginx, put the backend server on port 6001.

## Frontend Setup
1. **Open a new terminal and navigate to the frontend directory:**
    ```sh
    cd frontend
    ```

2. **Install the required dependencies:**
    ```sh
    npm install
    ```
3. **Start the frontend server on port 9090:**
    ```sh
    npm start
    ```
## Usage

Once both the backend and frontend servers are running, you can access the application by navigating to [http://localhost:9090](http://localhost:9090) in your web browser.
You can also to try it out login with these info
- jean@gmail.com
- jean
- 5000

## Contributing

We welcome contributions! If you have suggestions for improvements or new features, feel free to open an issue or submit a pull request.

## Contact

For any inquiries or support, please contact the project maintainers at [coding.diligence@gmail.com](coding.diligence@gmail.com).
