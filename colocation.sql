-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 16, 2024 at 04:40 PM
-- Server version: 8.0.36-0ubuntu0.22.04.1
-- PHP Version: 8.1.2-1ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `colocation`
--

-- --------------------------------------------------------

--
-- Table structure for table `coloc`
--

CREATE TABLE `coloc` (
  `coloc_id` int NOT NULL,
  `admin_id` int NOT NULL,
  `coloc_code` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `coloc`
--

INSERT INTO `coloc` (`coloc_id`, `admin_id`, `coloc_code`) VALUES
(5, 0, 1000),
(6, 0, 2000),
(7, 0, 1234),
(8, 0, 50456),
(9, 0, 91876),
(10, 0, 6454),
(11, 0, 10000),
(12, 0, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `depense`
--

CREATE TABLE `depense` (
  `id` int NOT NULL,
  `coloc_id` int NOT NULL,
  `depense` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `depense`
--

INSERT INTO `depense` (`id`, `coloc_id`, `depense`, `description`) VALUES
(2, 5, '50.00', 'Utility bill'),
(14, 5, '1000', 'Paid'),
(16, 11, '0', 'Initial depense entry'),
(17, 11, '1000', 'Pay'),
(18, 11, '-5000', 'Car'),
(21, 12, '100', 'Argent pour la soirée de demain'),
(22, 12, '-35', 'Vin'),
(23, 12, '-50', 'Course');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int NOT NULL,
  `coloc_id` int NOT NULL,
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `coloc_id`, `mail`, `name`, `title`, `description`, `created_at`) VALUES
(34, 5, 'admin@admin.com', 'user2', 'Rangement', 'range ta chambre', '2024-06-12 20:19:07'),
(35, 5, 'admin@admin.com', 'user2', 'Repose toi', 'Faut que tu dorme frerot', '2024-06-12 20:19:55'),
(36, 5, 'admin@admin.com', 'test', 'job', 'trouver un job', '2024-06-12 22:18:12'),
(41, 6, 'user@user.com', 'user', 'Vaisselle', 'Fais la vaisselle', '2024-06-13 08:55:16'),
(42, 5, 'admin@admin.com', 'admin', 'Rangement', 'range ta chambre', '2024-06-13 11:01:32'),
(44, 5, 'test@test.com', 'test', 'Payer l\'electricite', 'a ton tour test', '2024-06-14 10:37:45'),
(45, 12, 'jean@gmail.com', 'Camile', 'Courses', 'Besoin de pâtes, crème fraiche et lardon ', '2024-06-15 20:23:44'),
(46, 12, 'jean@gmail.com', 'Anais', 'Ménage', 'Le salon et la cuisine', '2024-06-15 20:24:15'),
(47, 12, 'jean@gmail.com', 'Jean', 'Course', 'Un vin du caviste', '2024-06-15 20:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `coloc_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `mail`, `password`, `name`, `coloc_id`) VALUES
(0, 'admin@admin.com', '$2y$10$Yfii3M9e68h1/tmVG2ivZ.HO1FljEywK8FavQEUUA0Om4u7OecnAC', 'admin', 5),
(7, 'user@user.com', '$2y$10$J6ifiDOTSk8.CgKHCrenzO5zU7Pt1nnwwYMIV.uxnHWNrODs6FIPu', 'user', 6),
(8, 'test@mail.com', '$2y$10$mWRbp4.gWIOgmLdCJeJNQ.U3ZARNK4UdtEtybcJiqiuZt1DCraoHi', 'test', 7),
(9, 'user2@user2.com', '$2y$10$Y0whTcW35qTJDwzrt2Ezhuu9L0DrLJnpiogUNhaHcRcTGjAZYQz3W', 'user2', 5),
(10, 'seur@seur.com', '$2y$10$tNiusYHaz9iF1F3hOMy49uQIit1rIQAO8dqdA.oDitrKnT.3v5x3u', 'seur', 6),
(11, 'ricassou@gmail.com', '$2y$10$Qxa1nB0bTc3YPXEGCFOkheqqd7gENCSKPzXDy10YwbPqlM36XYXL.', 'ricassou', 6),
(12, 'test@test.com', '$2y$10$0KsaAjfssfcXYGWwlv7v8eIcwkp6pOMp4qecxIShr/s6NZ4q4l33a', 'test', 5),
(13, 'user3@user.com', '$2y$10$izeawH9Zc4VUaNh2QWQMoOFP5q/gBpYO5TONhzom5y/uC1QS1dTeK', 'user3', 8),
(14, 'test3@test3.com', '$2y$10$6bJPzbWW93R5nf0ABE/niu8MKosxeb7qfysfj.YGw.4W8u5TV7Pkq', 'test3', 5),
(15, 'test4@test4.com', '$2y$10$M7J8btz2dzSMG6QHPctsl.f6GK/HH9NTnYSCdhjt5WRudB8G97jC2', 'test4', 9),
(16, 'qsdqs', '$2y$10$LdoKp923qrF6SLkYQ8hXjOMMS3ldjAMLEe6kJuCViAxsGX.VanmCK', 'dqsdqsd', 10),
(17, 'vaea@gmail.com', '$2y$10$sCWdQQV.Ngb0JckrXypETeQH8b2hHkl7fYnayQ3e2B3zTZ2BZK.3S', 'vaea', 11),
(18, 'jean@gmail.com', '$2y$10$wavPP4IZe4iC00lyJwd0zeRR4Az1cwNMtDEQxB7XVnejlywdWFscS', 'Jean', 12),
(19, 'anais@gmail.com', '$2y$10$EmUfVIhgw8RsMCNSqHn7Z.2xeEsJH63azFTJuO1XDhAqYEtyzHfRG', 'Anais', 12),
(20, 'camile@gmail.com', '$2y$10$882ro2lj9/5hcpsggxfFuOs.x8pEMKeAOoKdIC9KYuYcfARWxSI42', 'Camile', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coloc`
--
ALTER TABLE `coloc`
  ADD PRIMARY KEY (`coloc_id`);

--
-- Indexes for table `depense`
--
ALTER TABLE `depense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coloc_id` (`coloc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coloc`
--
ALTER TABLE `coloc`
  MODIFY `coloc_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `depense`
--
ALTER TABLE `depense`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`coloc_id`) REFERENCES `coloc` (`coloc_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
