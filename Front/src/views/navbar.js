import getCookie from '../script/getCookie';

const navBar = () => {
  const isLoggedIn = getCookie('status') !== null;

  const logoutButton = isLoggedIn ? '<button class="btn btn-outline-light ms-auto" id="logoutButton">Logout</button>' : '';

  return `
    <nav class="navbar p-4 bg-dark">
      <div class="position-absolute top-50 start-50 translate-middle">
        <a class="navbar-brand text-white" href="home">
          <img src="https://i.pinimg.com/564x/d5/7b/75/d57b75ed30371746f70364dd53bc4511.jpg" alt="" width="50" height="40" class="d-inline-block align-text navLogo">
          Coco Loca
        </a>
      </div>
      ${logoutButton}
    </nav>
  `;
};

export default navBar;
