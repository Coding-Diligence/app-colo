const calendarView = () => `
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header bg-primary text-white">
          Calendar
        </div>
        <div class="card-body">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>
`;

export default calendarView;
