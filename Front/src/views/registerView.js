const register = () => `
<section class="vh-100">
  <div class="container py-5 h-100 container-register">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col col-xl-10">
        <div class="card" style="border-radius: 1rem;">
          <div class="row g-0">
            <div class="col-md-6 col-lg-5 d-none d-md-block">
              <img src="https://i.pinimg.com/736x/52/27/e9/5227e94932c8c1251b87454b4e235f8c.jpg"
                width="643" height="950" alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
            </div>
            <div class="col-md-6 col-lg-7 d-flex align-items-center">
              <div class="card-body p-4 p-lg-5 text-black">

                <form>

                  <div class="d-flex align-items-center mb-3 pb-1">
                    <img src="https://i.pinimg.com/564x/d5/7b/75/d57b75ed30371746f70364dd53bc4511.jpg" alt="" width="50" height="40" class="d-inline-block align-text">
                  </div>

                  <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Créer une Colocation</h5>

                  <div data-mdb-input-init class="form-outline mb-3">
                    <input type="email" id="emailInput" class="form-control form-control-lg" placeholder="Mail"/>
                  </div>

                  <div data-mdb-input-init class="form-outline mb-3">
                    <input type="password" id="passwordInput" class="form-control form-control-lg" placeholder="Mot de Passe"/>
                  </div>

                  <div data-mdb-input-init class="form-outline mb-3">
                    <input type="name" id="nameInput" class="form-control form-control-lg" placeholder="Nom d'utilisateur"/>
                  </div>

                  <div data-mdb-input-init class="form-outline mb-3">
                    <input type="code" id="codeInput" class="form-control form-control-lg" placeholder="Code Colocation"/>
                  </div>

                  <div class="text-danger error"></div>
                  
                  <div class="pt-1 mb-4">
                    <button data-mdb-button-init data-mdb-ripple-init class="btn btn-dark btn-lg btn-block" type="button">Login</button>
                  </div>

                  
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
`;

export default register;
