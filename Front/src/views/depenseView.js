const depenseView = () => `
<div class="d-flex justify-content-between align-items-center p-2">
  <h3 class="text-white m-0"> Depense Management </h3>
  <button class="btn btn-primary addButton">Add Depense</button>
</div>
<div class="depense-form-container d-none">
  <form id="depenseForm" class="p-3">
    <div class="mb-3">
      <label for="depenseAmount" class="form-label text-white">Depense Amount</label>
      <input type="number" class="form-control" id="depenseAmount" name="depenseAmount" required>
    </div>
    <div class="mb-3">
      <label for="depenseDescription" class="form-label text-white">Depense Description</label>
      <textarea class="form-control" id="depenseDescription" name="depenseDescription" rows="3" required></textarea>
    </div>
    <div class="mb-3">
      <label for="depenseType" class="form-label text-white">Depense Type</label>
      <select class="form-select" id="depenseType" name="depenseType">
        <option class="text-success" value="positive">Positive (+)</option>
        <option class="text-danger" value="negative">Negative (-)</option>
      </select>
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
  </form>
</div>
<div class="card total-depense-card mt-3">
  <div class="card-body">
    <h5 class="card-title">Total Depense</h5>
    <h3 class="card-text" id="totalDepenseAmount">Loading...</h3>
  </div>
</div>
<div class="depense-container mt-3"></div>
`;

export default depenseView;
