const menu = () => `
  <div class="container overflow-hidden text-center position-absolute top-50 start-50 translate-middle">
    <div class="row gx-4 my-5 d-flex justify-content-between align-items-center">
      <!-- Premiere colonne -->
      <div class="calendrierMenu btn col-7 my-3 custom-padding text-black rounded-custom" data-path="calendrier">
          <h3>[ Calendrier ]</h3>
      </div>
      <div class="tableau btn col-4 my-3 custom-padding text-black rounded-custom" data-path="task">
          <h3>[ Tâche ]</h3>
      </div>
    </div>
    <div class="row gx-4 mt-5 d-flex justify-content-between my-5 align-items-center">
      <div class="depenseMenu btn col-4 my-3 custom-padding text-black rounded-custom" data-path="profil">
          <h3>[ Profil ]</h3>
      </div>
      <div class="budget btn col-7 my-3 custom-padding text-black rounded-custom" data-path="depense">
          <h3>[ Dépense ]</h3>
      </div>
    </div>
  </div>
`;

export default menu;
