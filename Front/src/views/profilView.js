const profilView = (user, tasks) => `
<div class="container mt-5">
    <h2 class="text-white">User Profile</h2>
    
    <div class="row mt-5">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Profile Information</h5>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" id="name" class="form-control" value="${user ? user.name : ''}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" id="email" class="form-control" value="${user ? user.mail : ''}" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Tasks Assigned to You</h5>
                    ${tasks.length > 0 ? `
                        <ul class="list-group">
                            ${tasks.map((task) => `
                                <li class="list-group-item">
                                    <strong>${task.title}</strong>
                                    <p>${task.description}</p>
                                    <small>Created at: ${task.created_at}</small>
                                </li>
                            `).join('')}
                        </ul>
                    ` : '<p>No tasks found.</p>'}
                </div>
            </div>
        </div>
    </div>
</div>
`;

export default profilView;
