const home = () => `
  <div id="accueil" class="position-absolute top-50 start-50 translate-middle">
    <div class="d-flex justify-content-around">

      <div class="card m-5" style="width: 20rem; height: 22rem">
        <div class="card-body bg-dark text-white rounded">
          <h5 class="card-title text-danger">Envie aventures</h5>
          <p class="card-text">Envie de vivre avec des gens, de partager de nouvelles expériences, de vous amuser et peut-être même de trouver votre prochain chez-vous ? Ne cherchez pas plus loin.</p>
          <a href="login" class="btn btn-primary btn-home">Rejoindre une Colocation</a>
        </div>
      </div>

      <div class="card m-5" style="width: 20rem; height: 22rem">
        <div class="card-body bg-dark text-white rounded">
          <h5 class="card-title text-danger">Propriétaire ?</h5>
          <p class="card-text">Vous possédez un bien que vous souhaitez louer à plusieurs personnes pour vivre ensemble ou en colocation avec vous ? Nous avons tout ce dont vous avez besoin ici.</p>
          <a href="register" class="btn btn-primary btn-home">Créer une Colocation</a>
        </div>
      </div>  

      <div class="card m-5" style="width: 20rem; height: 22rem">
        <div class="card-body bg-dark text-white rounded">
          <h5 class="card-title text-danger">Rejoindre la communauté</h5>
          <p class="card-text">Vous souhaitez rejoindre notre communauté ? Créez un compte Colo Loca pour explorer les colocations disponibles et rejoindre celle qui vous convient le mieux.</p>
          <a href="registerUser" class="btn btn-primary btn-home">Créer un compte Colo Loca</a>
        </div>
      </div>  
    </div>  
  </div>
`;

export default home;
