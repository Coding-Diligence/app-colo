const task = () => `
<div class="d-flex justify-content-between align-items-center p-2">
  <h3 class="text-white m-0"> Task Management </h3>
  <button class="btn btn-primary addButton">Add Task</button>
</div>
<div class="task-form-container d-none">
  <form id="taskForm" class="p-3">
    <div class="mb-3">
      <label for="title" class="form-label text-white">Title</label>
      <input type="text" class="form-control" id="title" name="title" required>
    </div>
    <div class="mb-3">
      <label for="description" class="form-label text-white">Description</label>
      <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
    </div>
    <div class="mb-3">
      <label for="target" class="form-label text-white">Target</label>
      <select class="form-control" id="target" name="target" required></select> <!-- Add select field for target -->
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
  </form>
</div>
<div class="task-container"></div>
`;

export default task;
