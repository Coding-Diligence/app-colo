export default function moveToNext(event, nextInputId) {
  if (event.key === 'Enter') {
    event.preventDefault();
    document.getElementById(nextInputId).focus();
  }
}
