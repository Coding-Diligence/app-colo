import Router from './Router';
import Home from './controllers/home';
import Login from './controllers/login';
import Register from './controllers/register';
import RegisterUser from './controllers/registerUser';
import Menu from './controllers/menu';
import Redirection from './controllers/redirection';
import Task from './controllers/task';
import Profile from './controllers/profil';
import Depense from './controllers/depense';
import Calendar from './controllers/calendar';

import './index.scss';

const routes = [
  {
    url: '/',
    controller: Redirection
  },
  {
    url: '/home',
    controller: Home
  },
  {
    url: '/login',
    controller: Login
  },
  {
    url: '/register',
    controller: Register
  },
  {
    url: '/registerUser',
    controller: RegisterUser
  },
  {
    url: '/menuColocation',
    controller: Menu
  },
  {
    url: '/task',
    controller: Task
  },
  {
    url: '/profil',
    controller: Profile
  },
  {
    url: '/depense',
    controller: Depense
  },
  {
    url: '/calendrier',
    controller: Calendar
  }
];

new Router(routes);
