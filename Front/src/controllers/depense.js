import axios from 'axios';
import depenseView from '../views/depenseView';
import viewNav from '../views/navbar';
import getCookie from '../script/getCookie';
import deleteAllCookies from '../script/deleteCookie';

axios.defaults.baseURL = 'http://localhost:6001';

class Depense {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;
    this.run();
  }

  render() {
    return `
      ${viewNav()}
      <div class="depense">
        ${depenseView()}
      </div>
    `;
  }

  run() {
    this.el.innerHTML = this.render();

    this.loadDepenses();

    const addButton = this.el.querySelector('.addButton');
    addButton.addEventListener('click', () => {
      this.showDepenseForm();
    });

    const depenseForm = this.el.querySelector('#depenseForm');
    depenseForm.addEventListener('submit', (event) => {
      event.preventDefault();
      this.submitDepenseForm();
    });

    const depenseContainer = document.querySelector('.depense-container');
    depenseContainer.addEventListener('click', (event) => {
      if (event.target.classList.contains('btn-delete')) {
        const depenseId = event.target.getAttribute('data-id');
        this.deleteDepense(depenseId);
      }
    });

    this.setupLogoutButton();
  }

  loadDepenses() {
    const colocId = getCookie('coloc_id');
    axios.get(`/depense/${colocId}`)
      .then((response) => {
        const depenseContainer = document.querySelector('.depense-container');
        if (Array.isArray(response.data)) {
          depenseContainer.innerHTML = '';

          let totalDepense = 0;

          response.data.forEach((depense) => {
            const depenseAmount = parseFloat(depense.depense);
            totalDepense += depenseAmount;

            const card = document.createElement('div');
            card.classList.add('card', 'my-2');
            card.style.backgroundColor = depenseAmount >= 0 ? 'lightgreen' : 'lightcoral';
            card.innerHTML = `
              <div class="card-body">
                <h4 class="card-title">${depenseAmount}</h4>
                <p class="card-text">${depense.description}</p>
                <button type="button" class="btn btn-danger btn-sm btn-delete" data-id="${depense.id}">Delete</button>
              </div>
            `;
            depenseContainer.appendChild(card);
          });

          const totalDepenseAmountElement = document.getElementById('totalDepenseAmount');
          totalDepenseAmountElement.textContent = totalDepense.toFixed(2);
          totalDepenseAmountElement.style.color = totalDepense >= 0 ? 'green' : 'red';
        }
      });
  }

  showDepenseForm() {
    const depenseFormContainer = this.el.querySelector('.depense-form-container');
    depenseFormContainer.classList.remove('d-none');
  }

  submitDepenseForm() {
    const depenseAmount = parseFloat(this.el.querySelector('#depenseAmount').value);
    const depenseDescription = this.el.querySelector('#depenseDescription').value;
    const depenseType = this.el.querySelector('#depenseType').value;
    const colocId = getCookie('coloc_id');

    if (depenseAmount && depenseDescription) {
      axios.post('/depense', {
        colocId,
        depense: depenseType === 'positive' ? depenseAmount : -depenseAmount,
        description: depenseDescription
      })
        .then(() => {
          this.loadDepenses();
          this.el.querySelector('#depenseForm').reset();
          const depenseFormContainer = this.el.querySelector('.depense-form-container');
          depenseFormContainer.classList.add('d-none');
        });
    }
  }

  deleteDepense(id) {
    axios.delete(`/depense/${id}`)
      .then(() => {
        this.loadDepenses();
      });
  }

  setupLogoutButton() {
    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
  }
}

export default Depense;
