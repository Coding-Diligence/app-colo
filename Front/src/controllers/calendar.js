import viewNav from '../views/navbar';
import calendarView from '../views/calendarView';
import deleteAllCookies from '../script/deleteCookie';

class Calendar {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;
    if (this.el) {
      this.run();
    }
  }

  render() {
    return `
      ${viewNav()}
      <div class="container mt-4">
        ${calendarView()}
      </div>
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    this.setupLogoutButton();
  }

  setupLogoutButton() {
    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
  }
}

export default Calendar;
