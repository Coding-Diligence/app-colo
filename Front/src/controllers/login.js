import axios from 'axios';
import navBar from '../views/navbar';
import login from '../views/login';
import moveToNext from '../script/moveToNext';
import deleteAllCookies from '../script/deleteCookie';

const Login = class {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;
    this.run();
  }

  render() {
    return `
    ${navBar()}
    ${login()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
    const loginButton = this.el.querySelector('.btn-block');
    loginButton.addEventListener('click', () => {
      const emailInput = this.el.querySelector('#emailInput').value;
      const passwordInput = this.el.querySelector('#passwordInput').value;
      const codeInput = this.el.querySelector('#codeInput').value;
      const Error = this.el.querySelector('.error');

      axios.post('http://localhost:6001/login', {
        mail: emailInput,
        password: passwordInput,
        coloc_code: codeInput
      }).then((response) => {
        if (response.data.status === 'success') {
          document.cookie = response.data.cookie1;
          document.cookie = response.data.cookie2;
          document.cookie = response.data.cookie3;
          document.cookie = response.data.cookie4;
          document.cookie = response.data.cookie5;
          window.location.href = '/menuColocation';
        } else {
          Error.innerHTML = response.data.message;
        }
      });
    });

    const emailInput = this.el.querySelector('#emailInput');
    const passwordInput = this.el.querySelector('#passwordInput');
    const codeInput = this.el.querySelector('#codeInput');

    emailInput.addEventListener('keydown', (event) => moveToNext(event, 'passwordInput'));
    passwordInput.addEventListener('keydown', (event) => moveToNext(event, 'codeInput'));

    codeInput.addEventListener('keydown', (event) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        loginButton.click();
      }
    });
  }
};

export default Login;
