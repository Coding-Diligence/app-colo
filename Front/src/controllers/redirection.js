const Redirection = class {
  constructor(params) {
    this.params = params;
    this.run();
  }

  run() {
    window.location = '/home';
  }
};

export default Redirection;
