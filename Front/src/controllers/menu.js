import navBar from '../views/navbar';
import menu from '../views/menu';
import getCookie from '../script/getCookie';
import deleteAllCookies from '../script/deleteCookie';

class Menu {
  constructor(params) {
    this.params = params;
    this.render();
    this.setupEventListeners();
  }

  render() {
    const el = document.querySelector('#home');
    el.innerHTML = `
      ${navBar()}
      ${menu()}
    `;
    this.el = el;
  }

  setupEventListeners() {
    const status = getCookie('status');

    this.el.addEventListener('click', (event) => {
      const target = event.target.closest('[data-path]');
      if (!target) return;

      const path = target.getAttribute('data-path');

      switch (path) {
        case 'calendrier':
        case 'task':
        case 'depense':
        case 'profil':
          if (status === 'success') {
            window.location.href = `/${path}`;
          }
          break;
        case 'logout':
          deleteAllCookies();
          window.location.href = '/home';
          break;
        default:
          break;
      }
    });

    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
  }
}

export default Menu;
