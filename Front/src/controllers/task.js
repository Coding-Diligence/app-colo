import axios from 'axios';
import task from '../views/taskView';
import viewNav from '../views/navbar';
import getCookie from '../script/getCookie';
import deleteAllCookies from '../script/deleteCookie';

axios.defaults.baseURL = 'http://localhost:6001';

class Task {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;
    this.run();
  }

  render() {
    return `
      ${viewNav()}
      <div class="task">
        ${task()}
      </div>
    `;
  }

  run() {
    this.el.innerHTML = this.render();

    this.loadTasks();

    const addButton = this.el.querySelector('.addButton');
    addButton.addEventListener('click', () => {
      this.showTaskForm();
    });

    const taskForm = this.el.querySelector('#taskForm');
    taskForm.addEventListener('submit', (event) => {
      event.preventDefault();
      this.submitTaskForm();
    });

    this.setupLogoutButton();
  }

  loadTasks() {
    const id = getCookie('coloc_id');
    axios.get(`/task/${id}`).then((response) => {
      const taskContainer = document.querySelector('.task-container');
      if (Array.isArray(response.data)) {
        taskContainer.innerHTML = '';

        response.data.forEach((element) => {
          const taskId = element.task_id;
          const card = document.createElement('div');
          card.classList.add('card', 'my-2');
          card.style.backgroundColor = 'white';
          card.innerHTML = `
            <div class="card-body">
              <div class="card-header">
                <h4 class="card-title" style="font-weight: bold;">${element.title}</h4>
                <span class="card-name text-black">Personne qui doit faire la tache : <a class="text-danger">${element.name}</a></span>
              </div>
              <p class="card-text" style="margin-top: 10px;">${element.description}</p>
              <button type="button" class="btn-close btn-close-red" aria-label="Close" data-id="${taskId}" style="position: absolute; top: 0; left: 0; background-color: red; color: white;"></button>
            </div>
          `;
          taskContainer.appendChild(card);
        });

        taskContainer.addEventListener('click', (event) => {
          if (event.target.classList.contains('btn-close')) {
            const taskId = event.target.getAttribute('data-id');
            this.deleteTask(taskId);
          }
        });
      }
    });
  }

  showTaskForm() {
    const taskFormContainer = this.el.querySelector('.task-form-container');
    taskFormContainer.classList.remove('d-none');

    this.loadUsers();
  }

  loadUsers() {
    const id = getCookie('coloc_id');
    axios.get(`/userColoc/${id}`).then((response) => {
      const targetSelect = this.el.querySelector('#target');
      targetSelect.innerHTML = '';

      response.data.forEach((user) => {
        const option = document.createElement('option');
        option.value = user.name;
        option.textContent = user.name;
        targetSelect.appendChild(option);
      });
    });
  }

  submitTaskForm() {
    const title = this.el.querySelector('#title').value;
    const description = this.el.querySelector('#description').value;
    const name = this.el.querySelector('#target').value;
    const mail = getCookie('mail');
    const colocId = getCookie('coloc_id');

    if (title && description) {
      axios.post('/tasks', {
        colocId,
        mail,
        title,
        description,
        name
      }).then((response) => {
        if (response.data.status === 'success') {
          this.loadTasks();
          this.el.querySelector('#taskForm').reset();
          const taskFormContainer = this.el.querySelector('.task-form-container');
          taskFormContainer.classList.add('d-none');
        }
      });
    }
  }

  deleteTask(id) {
    axios.delete(`/task/${id}`)
      .then(() => {
        window.location.reload();
      });
  }

  setupLogoutButton() {
    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
  }
}

export default Task;
