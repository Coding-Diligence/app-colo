import home from '../views/home';
import navBar from '../views/navbar';
import getCookie from '../script/getCookie';

const Home = class {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;

    const status = getCookie('status');

    if (status === 'success') {
      window.location.href = '/menuColocation';
    } else {
      this.run();
    }
  }

  render() {
    return `
      ${navBar()}
      ${home()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Home;
