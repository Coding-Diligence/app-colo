import axios from 'axios';
import RegisterUserView from '../views/registerUserView';
import viewNav from '../views/navbar';
import moveToNext from '../script/moveToNext';
import deleteAllCookies from '../script/deleteCookie';

const RegisterUser = class {
  constructor(params) {
    this.el = document.querySelector('#home');
    this.params = params;
    this.run();
  }

  render() {
    return `
    ${viewNav()}
    ${RegisterUserView()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();

    const registerUserButton = this.el.querySelector('.btn-block');
    const logoutButton = document.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
    registerUserButton.addEventListener('click', () => {
      const emailInput = this.el.querySelector('#emailInput').value;
      const passwordInput = this.el.querySelector('#passwordInput').value;
      const nameInput = this.el.querySelector('#nameInput').value;
      const codeInput = this.el.querySelector('#codeInput').value;
      const Error = this.el.querySelector('.error');

      axios.post('http://localhost:6001/registerUser', {
        mail: emailInput,
        password: passwordInput,
        name: nameInput,
        coloc_code: codeInput
      }).then((response) => {
        if (response.data.status === 'success') {
          document.cookie = response.data.cookie1;
          document.cookie = response.data.cookie2;
          document.cookie = response.data.cookie3;
          document.cookie = response.data.cookie4;
          window.location.href = '/menuColocation';
        } else {
          Error.innerHTML = response.data.message;
        }
      });
    });

    const emailInput = this.el.querySelector('#emailInput');
    const passwordInput = this.el.querySelector('#passwordInput');
    const nameInput = this.el.querySelector('#nameInput');
    const codeInput = this.el.querySelector('#codeInput');

    emailInput.addEventListener('keydown', (event) => moveToNext(event, 'passwordInput'));
    passwordInput.addEventListener('keydown', (event) => moveToNext(event, 'nameInput'));
    nameInput.addEventListener('keydown', (event) => moveToNext(event, 'codeInput'));

    codeInput.addEventListener('keydown', (event) => {
      if (event.key === 'Enter') {
        event.preventDefault();
        registerUserButton.click();
      }
    });
  }
};

export default RegisterUser;
