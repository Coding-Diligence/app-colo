import axios from 'axios';
import profilView from '../views/profilView';
import navBar from '../views/navbar';
import getCookie from '../script/getCookie';
import deleteAllCookies from '../script/deleteCookie';

axios.defaults.baseURL = 'http://localhost:6001';

class Profil {
  constructor(params) {
    this.params = params;
    this.el = null;
    this.user = null;
    this.tasks = [];
    this.run();
  }

  render() {
    return `
      ${navBar()}
      ${profilView(this.user, this.tasks)}
    `;
  }

  run() {
    document.addEventListener('DOMContentLoaded', () => {
      this.el = document.querySelector('#home');
      const userId = getCookie('user_id');
      this.loadUserData(userId);
    });
  }

  loadUserData(userId) {
    axios.get(`/profil/${userId}`)
      .then((response) => {
        if (response.data && response.data.user) {
          this.user = response.data.user;
          this.tasks = response.data.tasks || [];
          this.el.innerHTML = this.render();
          this.setupLogoutListener();
        }
      });
  }

  setupLogoutListener() {
    const logoutButton = this.el.querySelector('#logoutButton');
    if (logoutButton) {
      logoutButton.addEventListener('click', () => {
        deleteAllCookies();
        window.location.href = '/home';
      });
    }
  }
}

export default Profil;
