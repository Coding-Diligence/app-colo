<?php

namespace App\Models;

use \PDO;

class TaskModel extends SqlConnect {
    public function get($id) {
        $req = $this->db->prepare("SELECT * FROM tasks WHERE task_id=:id");
        $req->execute(["id" => $id]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }
    
    public function getAll($coloc_id) {
        $req = $this->db->prepare("SELECT * FROM tasks WHERE coloc_id=:coloc_id");
        $req->execute(["coloc_id" => $coloc_id]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function getAllByMail($mail) {
        $req = $this->db->prepare("SELECT * FROM tasks WHERE mail=:mail");
        $req->execute(["mail" => $mail]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function getByName($name) {
        $req = $this->db->prepare("SELECT * FROM tasks WHERE name=:name");
        $req->execute(["name" => $name]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function delete($id) {
        $req = $this->db->prepare("DELETE FROM tasks WHERE task_id=:id");
        $req->execute(["id" => $id]);
        return $req->rowCount() > 0;
    }

    public function add($task) {
        $query = "INSERT INTO tasks (coloc_id, mail, name, title, description) VALUES (:coloc_id, :mail, :name, :title, :description)";
        $req = $this->db->prepare($query);
        $req->execute([
            'coloc_id' => $task['coloc_id'],
            'mail' => $task['mail'],
            'name' => $task['name'],
            'title' => $task['title'],
            'description' => $task['description']
        ]);
    }
}
