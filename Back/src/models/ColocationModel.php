<?php

namespace App\Models;

use \PDO;
use stdClass;

class ColocationModel extends SqlConnect {
    public function get($coloc_id) {
        $req = $this->db->prepare("SELECT * FROM coloc WHERE coloc_id=:coloc_id");
        $req->execute(["coloc_id" => $coloc_id]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function checkColocCodeAndId($coloc_id, $coloc_code) {
        $req = $this->db->prepare("SELECT * FROM coloc WHERE coloc_id=:coloc_id AND coloc_code=:coloc_code");
        $req->execute(['coloc_id' => $coloc_id, 'coloc_code' => $coloc_code]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function checkColocCode($coloc_code) {
        $req = $this->db->prepare("SELECT COUNT(*) FROM coloc WHERE coloc_code=:coloc_code");
        $req->execute(['coloc_code' => $coloc_code]);
        return $req->fetchColumn() > 0;
    }

    public function getByColocCode($coloc_code) {
        $req = $this->db->prepare("SELECT * FROM coloc WHERE coloc_code=:coloc_code");
        $req->execute(["coloc_code" => $coloc_code]);
        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function delete($coloc_id) {
        $req = $this->db->prepare("DELETE FROM coloc WHERE coloc_id = :coloc_id");
        $req->execute(["coloc_id" => $coloc_id]);
    }

    public function add($data) {
        $query = "INSERT INTO coloc (admin_id, coloc_code) VALUES (:admin_id, :coloc_code)";
        $req = $this->db->prepare($query);
        $req->execute([
            'admin_id' => $data['admin_id'],
            'coloc_code' => $data['coloc_code']
        ]);
    }
}
