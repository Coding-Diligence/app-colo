<?php

namespace App\Models;

use \PDO;

class DepenseModel extends SqlConnect {
    public function getAll($coloc_id) {
        $req = $this->db->prepare("SELECT * FROM depense WHERE coloc_id=:coloc_id");
        $req->execute(["coloc_id" => $coloc_id]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function delete($id) {
        $req = $this->db->prepare("DELETE FROM depense WHERE id=:id");
        $req->execute(["id" => $id]);
        return $req->rowCount() > 0;
    }

    public function add($depense) {
        $query = "INSERT INTO depense (coloc_id, depense, description) VALUES (:coloc_id, :depense, :description)";
        $req = $this->db->prepare($query);
        $req->execute([
            'coloc_id' => $depense['coloc_id'],
            'depense' => $depense['depense'],
            'description' => $depense['description']
        ]);
    }
}
