<?php

namespace App\Models;

use \PDO;

class UserModel extends SqlConnect {
    public function get($id) {
        $req = $this->db->prepare("SELECT * FROM users WHERE id=:id");
        $req->execute(["id" => $id]);
        return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
    }

    public function getByEmail($mail) {
        $req = $this->db->prepare("SELECT * FROM users WHERE mail=:mail");
        $req->execute(["mail" => $mail]);
        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }
    public function checkMail($mail) {
        $req = $this->db->prepare("SELECT id FROM users WHERE mail=:mail");
        $req->execute(["mail" => $mail]);
        $user = $req->fetch(PDO::FETCH_ASSOC);
        return $user !== false;
    }

    public function getUsersByColocId($colocId) {
      $req = $this->db->prepare("SELECT name FROM users WHERE coloc_id = :colocId");
      $req->execute(['colocId' => $colocId]);
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : null;
  }  

    public function getByEmailAndPassword($mail, $password) {
        $req = $this->db->prepare("SELECT * FROM users WHERE mail=:mail AND password=:password");
        $req->execute(['mail' => $mail, 'password' => $password]);
        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }

    public function delete(int $id) {
        $req = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $req->execute(["id" => $id]);
    }

    public function add($data) {
        $query = "INSERT INTO users (mail, password, name, coloc_id) VALUES (:mail, :password, :name, :coloc_id)";
        $req = $this->db->prepare($query);
        $req->execute([
            'mail' => $data['mail'],
            'password' => $data['password'],
            'name' => $data['name'],
            'coloc_id' => $data['coloc_id']
        ]);
    }

    public function getLast() {
        $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
        $req->execute();

        return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }
}
