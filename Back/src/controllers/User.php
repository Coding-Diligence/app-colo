<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class User extends Controller {
    protected object $user;

    public function __construct($param) {
        $this->user = new UserModel();
        parent::__construct($param);
    }

    public function getUser() {
        return $this->user->get(intval($this->params['id']));
    }
}
