<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\DepenseModel;

class Depense extends Controller {
    protected $depenseModel;

    public function __construct($params) {
        $this->depenseModel = new DepenseModel();
        parent::__construct($params);
    }

    public function deleteDepense() {
        $depense_id = $this->params['id'];
        $deleteDepense = $this->depenseModel->delete($depense_id);
    }

    public function getDepense() {
        $coloc_id = $this->params['id'];
        $depenses = $this->depenseModel->getAll($coloc_id);

        if ($depenses === null) {
            echo json_encode(['depense' => 'none', 'description' => 'none']);
            return;
        }

        echo json_encode($depenses);
    }
}
