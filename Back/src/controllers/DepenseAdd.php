<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\DepenseModel;

class DepenseAdd extends Controller {
  protected $depenseModel;

  public function __construct($params) {
    $this->depenseModel = new DepenseModel();
    parent::__construct($params); 
  }

  public function postDepenseAdd() {
    $coloc_id = $this->body['colocId'] ?? '';
    $depense = $this->body['depense'] ?? '';
    $description = $this->body['description'] ?? '';

    if (empty($coloc_id) || empty($depense) || empty($description)) {
      echo json_encode(['status' => 'fail', 'message' => 'All fields are required']);
      return;
    }

    $newDepense = [
      'coloc_id' => $coloc_id,
      'depense' => $depense,
      'description' => $description,
    ];

    $addDepense = $this->depenseModel->add($newDepense);

    echo json_encode(['status' => 'success']);
  }
}
