<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;
use App\Models\TaskModel;

class Profil extends Controller {
    protected $userModel;
    protected $taskModel;

    public function __construct($params) {
        $this->userModel = new UserModel();
        $this->taskModel = new TaskModel();
        parent::__construct($params);
    }

    public function getProfil() {
        $id = $this->params['id'];
        $user = $this->userModel->get($id);

        if ($user === null || count($user) === 0) {
            echo json_encode(['error' => 'User not found']);
            return;
        }

        $user = $user[0];
        $tasks = $this->taskModel->getByName($user['name']);

        $taskCount = $tasks !== null ? count($tasks) : 0;

        echo json_encode([
            'user' => $user,
            'tasks' => $tasks
        ]);
    }
}
