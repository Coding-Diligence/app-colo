<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\ColocationModel;

class Coloc extends Controller {
    protected $colocationModel;

    public function __construct($params) {
        $this->colocationModel = new ColocationModel();
        parent::__construct($params);
    }

    public function getColoc() {
        $coloc_id = $this->params['id'];
        $colocation = $this->colocationModel->get($coloc_id);

        if ($colocation === null) {
            http_response_code(404);
            echo json_encode(['code' => '404', 'message' => 'Not Found']);
            return;
        }

        echo json_encode($colocation);
        return;
    }
}
