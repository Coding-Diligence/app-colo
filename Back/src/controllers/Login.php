<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;
use App\Models\ColocationModel;

class Login extends Controller {
    protected $userModel;
    protected $colocationModel;

    public function __construct($params) {
        $this->userModel = new UserModel();
        $this->colocationModel = new ColocationModel();
        parent::__construct($params);
    }

    public function postLogin() {
        $mail = $this->body['mail'];
        $password = $this->body['password'];
        $colocCode = $this->body['coloc_code'];

        if (empty($mail) || empty($password) || empty($colocCode)) {
            echo json_encode(['status' => 'fail', 'message' => 'All fields are required']);
            return;
        }

        $user = $this->userModel->getByEmail($mail);
        if (!$this->colocationModel->checkColocCodeAndId($user['coloc_id'], $colocCode)) {
            echo json_encode(['status' => 'fail', 'message' => 'Invalid Credentials']);
            return;
        }

        if ($user && password_verify($password, $user['password'])) {
            $name = $user['name'];
            $mail = $user['mail'];
            $coloc_id = $user['coloc_id'];
            $user_id = $user['id'];
            $time = time() + 86400;
            $expiresFormatted = gmdate('D, d M Y H:i:s \G\M\T', $time);
            $status = 'success';
            $cookieString1 = "status=$status; expires=$expiresFormatted; path=/";
            $cookieString2 = "mail=$mail; expires=$expiresFormatted; path=/";
            $cookieString3 = "name=$name; expires=$expiresFormatted; path=/";
            $cookieString4 = "coloc_id=$coloc_id; expires=$expiresFormatted; path=/";
            $cookieString5 = "user_id=$user_id; expires=$expiresFormatted; path=/";
            echo json_encode(['status' => 'success', 'cookie1' => $cookieString1, 'cookie2' => $cookieString2, 'cookie3' => $cookieString3, 'cookie4' => $cookieString4, 'cookie5' => $cookieString5]);
        } else {
            echo json_encode(['status' => 'fail', 'message' => 'Invalid credentials']);
        }
    }
}