<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class UserColoc extends Controller {
  protected $userModel;

  public function __construct($params) {
    $this->userModel = new UserModel();
    parent::__construct($params); 
  }

  public function getUserColoc() {
    $coloc_id = $this->params['id'];
    $users = $this->userModel->getUsersByColocId($coloc_id);
    echo json_encode($users);
  }
}
