<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;
use App\Models\ColocationModel;

class RegisterUser extends Controller {
    protected $userModel;
    protected $colocationModel;

    public function __construct($params) {
        $this->userModel = new UserModel();
        $this->colocationModel = new ColocationModel();
        parent::__construct($params);
    }

    public function postRegisterUser() {
        $mail = $this->body['mail'] ?? '';
        $password = $this->body['password'] ?? '';
        $name = $this->body['name'] ?? '';
        $colocCode = $this->body['coloc_code'] ?? '';

        if (empty($mail) || empty($password) || empty($name) || empty($colocCode)) {
            echo json_encode(['status' => 'fail', 'message' => 'All fields are required']);
            return;
        }

        if ($this->userModel->checkMail($mail)) {
            echo json_encode(['status' => 'fail', 'message' => 'Email already exists']);
            return;
        }

        if ($this->colocationModel->checkColocCode($colocCode)) {
            $colocation = $this->colocationModel->getByColocCode($colocCode);
            $colocId = $colocation['coloc_id'];

            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

            $user = [
                'mail' => $mail,
                'password' => $hashedPassword,
                'name' => $name,
                'coloc_id' => $colocId
            ];

            $this->userModel->add($user);

            $time = time() + 86400;
            $expiresFormatted = gmdate('D, d M Y H:i:s \G\M\T', $time);
            $status = 'success';
            $cookieString1 = "status=$status; expires=$expiresFormatted; path=/";
            $cookieString2 = "mail=$mail; expires=$expiresFormatted; path=/";
            $cookieString3 = "name=$name; expires=$expiresFormatted; path=/";
            $cookieString4 = "coloc_id=$coloc_id; expires=$expiresFormatted; path=/";
            echo json_encode(['status' => 'success', 'cookie1' => $cookieString1, 'cookie2' => $cookieString2, 'cookie3' => $cookieString3, 'cookie4' => $cookieString4]);
        } else {
            echo json_encode(['status' => 'fail', 'message' => 'Invalid colocCode']);
        }
    }
}
