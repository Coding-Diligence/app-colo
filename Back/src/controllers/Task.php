<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\TaskModel;

class Task extends Controller {
  protected $taskModel;

  public function __construct($params) {
    $this->taskModel = new TaskModel();
    parent::__construct($params);
  }

  public function deleteTask() {
    $task_id = $this->params['id'];
    $deleteTask = $this->taskModel->delete($task_id);
  }

  public function getTask() {
    $id = $this->params['id'];
    $task = $this->taskModel->getAll($id);

    if ($task === null) {
      echo json_encode(['title' => 'none', 'description' => 'none']);
      return;
    }

    echo json_encode($task);
  }
}