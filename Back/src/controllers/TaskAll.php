<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\TaskModel;

class TaskAll extends Controller {
  protected $taskModel;

  public function __construct($params) {
    $this->taskModel = new TaskModel();
    parent::__construct($params); 
  }

  public function postTaskAll() {
    $coloc_id = $this->body['colocId'] ?? '';
    $task_mail = $this->body['mail'] ?? '';
    $task_name = $this->body['name'] ?? '';
    $task_title = $this->body['title'] ?? '';
    $task_description = $this->body['description'] ?? '';

    if (empty($task_title) || empty($task_description) || empty($task_name)) {
      echo json_encode(['status' => 'fail', 'message' => 'All fields are required']);
      return;
    }

    $newTask = [
      'coloc_id' => $coloc_id,
      'mail' => $task_mail,
      'name' => $task_name,
      'title' => $task_title,
      'description' => $task_description,
    ];

    $addTask = $this->taskModel->add($newTask);
    echo json_encode(['status' => 'success']);
  }
}
