<?php

require 'vendor/autoload.php';

use App\Router;

use App\Controllers\User;
use App\Controllers\UserColoc;

use App\Controllers\Coloc;

use App\Controllers\TaskAll;
use App\Controllers\Task;

use App\Controllers\Login;
use App\Controllers\RegisterColoc;
use App\Controllers\RegisterUser;

use App\Controllers\Profil;

use App\Controllers\Depense;
use App\Controllers\DepenseAdd;

new Router([
    'user/:id' => User::class,
    'coloc/:id' => Coloc::class,
    'login/' => Login::class,
    'register/' => RegisterColoc::class,
    'registerUser/' => RegisterUser::class,
    'tasks' => TaskAll::class,
    'task/:id' => Task::class,
    'userColoc/:id' => UserColoc::class,
    'profil/:id' => Profil::class,
    'depense/:id' => Depense::class,
    'depense/' => DepenseAdd::class,
]);
